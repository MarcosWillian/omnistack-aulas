const express = require('express');

const app = express();

app.get('/', (request, response) => {
    return response.json({
        evento: 'Teste',
        usuario: 'Marcos'
    });
});

app.listen(3333);